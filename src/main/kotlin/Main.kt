/**
*@author: Daniel Reinosa Luque
*@version: "1.0-SNAPSHOT"
*/
import java.io.File
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.forEachLine
import kotlin.io.path.readLines
import kotlin.io.path.readText

/**
Implementacion de varables para los colores, el scanner y la variable error que retornará error del sistema
en caso de no funcionamiento de la aplicacion.
 */
val file = File(".\\Wordle\\src\\main\\resources\\wordList.txt")
data class rankList(val ID:Int,val nickName:String, val points:Int)

const val colorGris = "\u001b[37m"
const val colorGroc = "\u001b[33m"
const val colorVerd = "\u001b[32m"
const val colorRed = "\u001b[31m"
const val colorReset = "\u001b[0m"

val scanner = Scanner(System.`in`)
var error = false
var counter = 5



/**
*La siguiente funcion "colorRules", llama a la palabra de cinco letras con el tipo de dato String y la palabra generada
*aleatoriamente.
*Devolverá la comparacion de estas dos palabras retornando un String modificado por los colores.
*
*@param: selectedWord
*/
fun colorRules(selectedWord: String,solution:String):String{

    var msg = ""
    val wordList = mutableListOf<Word>{}
    val restList = mutableListOf<String>()

    if (selectedWord == solution) {
        win()
    }
    else for (i in 0..selectedWord.lastIndex) {
        // error
        if (selectedWord.length !=5) {
            println("La palabra no contiene 5 letras repite de nuevo.")
           error(true)
        }
        //win

        // print correcte
        else if (selectedWord[i] == solution[i]) {
            wordList[i] = ("$colorVerd${selectedWord[i]}$colorReset")
        }
        else if (selectedWord[i] != solution[i]){
            restList.add(solution[i].toString())
        }

   for(i in selectedWord.indices){
        // correcte pero a un altre lloc
         if ((selectedWord[i] != solution[i]) && (selectedWord[i] in solution) && (selectedWord[i] !in restList.toString())) {
            wordList[i] ="$colorGroc${selectedWord[i]}$colorReset"
             restList.add(selectedWord[i].toString())
         }
         // print incorrecte
         else if (selectedWord[i] !in solution)
             wordList[i] ="$colorGris${selectedWord[i]}$colorReset"
             restList.add(selectedWord[i].toString())
   }
    }
    return wordList.toString()
}

fun main() {
    Login()
    bienvenida()
    println(randomWord())
    val userWord = scanner.next().uppercase()
    when(userWord){
        "PLAY" ->{
            val solution = randomWord()
            for (j in 0..6){
                println("Intenta adivinar la palabra de cinco letras!")
                val selectedWord = scanner.next()
                if (counter != 0){
                    counter --
                    colorRules(selectedWord,solution)
                    println()
                    println("   -Vidas :$counter")
                }
                else if (counter == 0){
                    println("$colorRed Has perdido prueba de nuevo$colorReset")
                    println("La palabra correcta es:$solution")
                    error = true
                }
            }
        }
        "HELP" ->{
            help()
        }
        "RANK" ->{
            Ranking()
        }
    }
    while (userWord != "HELP"||userWord!="PLAY"||userWord!="RANK"){
        error = true
    }
}

/**
*LLama al fichero "wordList.txt" donde se encuentran las posibles palabras de respuesta, despues coge una palabra random
*y la retorna en forma de "randomWord".
*/
fun randomWord(fileName: String = ".\\Wordle\\src\\main\\resources\\wordList.txt"): String {
    val callFile = File(fileName)
    if (!callFile.exists()) {
        return "Error: file not found."
    }
    else{
        val readFile = callFile.readLines()
        if (readFile.isEmpty()) {
            return "Error: file is empty."
        }
        val randomWord = readFile.random()
        return randomWord
    }
}

fun Ranking(filePath:String =".\\Wordle\\src\\main\\resources\\ranking.txt"){
    val fileContent = File(filePath).readLines()
    for (i in fileContent.indices){
        val line = fileContent[i].split(",")
        println("${line[0]} ..... ${line[1]}")
    }

}


fun error(error: Boolean){
    while (error){
        println("${colorRed}Error detectat$colorReset")
        return main()
    }
}
/**
 *Llamada por el comado HELP se printara el siguiente texto.
 *Al finalizar se preguntara al usuario si quiere empezar la partida.
 */
fun help(){
    println(". . . . . . . . . . . . . . . . . . . . . . . . . . . W O R D L E . . . . . . . . . . . . . . . . . . . . . . . . . . .")
    println("El objetivo del juego es adivinar una palabra concreta de cinco letras en un máximo de seis intentos.\"")
    println("Escriba en primera línea una palabra de cinco letras de su elección")
    println("El jugador escribe en la primera línea una palabra de cinco letras de su elección e introduce su propuesta.\n" +
            "                 \n" +
            "                 Después de cada proposición, las letras aparecen en color:\n" +
            "                 $colorGris El fondo gris representa las letras que no están en la palabra buscada.$colorReset\n" +
            "                 $colorGroc El fondo amarillo representa las letras que se encuentran en otros lugares de la palabra. $colorReset \n" +
            "                 $colorVerd El fondo verde representa las letras que están en el lugar correcto en la palabra a encontrar.$colorReset \n")
    println("¿Quieres empezar la partida?$colorVerd S/N $colorReset")
    var answer = scanner.next().uppercase()
    if(answer == "S") return main()
    else if(answer == "N")answer == "HELP"
}
/**
*Printa el siguiente texto para dar la bienvenida
*/
fun bienvenida(){
    println("${colorGroc}. . . . . . . . . . . . . . . . . . . . . . Bienvenido a WORDLE . . . . . . . . . . . . . . . .$colorReset")
    println("Indica${colorVerd} PLAY$colorReset para jugar,${colorGroc}RANK$colorReset si deseas ver ranking o ${colorRed}HELP$colorReset si todavia no sabes las normas.")
    println("¡¡Buena suertee!!")
}
/**
*Printa el siguiente texto para indicar que has ganado.
*/
fun win(){
    println("${colorVerd}¡HAS GANADO CRACK!$colorReset")
}
fun Login(){
    println("Indica el teu nickname")
    val nickName = scanner.next()
    var idUsuario = 1
    val puntuacion = 0
        file.forEachLine {
            idUsuario++
        }
        val salida = "$idUsuario;$nickName;$puntuacion\n"
        file.appendText(salida)
}
fun lifeCounter(solution:String){


}
/*
Implementations

UF1 -> printar les lletres cada vegada
UF2 -> Repetir partida, fer max fun possibles (documentar en kdoc i fer test per a cada fun)
UF3 ->
 */
